﻿using Overlay.NET.Directx;
using System;
using static EDUtils.UI.Overlay.OverlayUI;

namespace EDUtils.UI.Overlay
{
    public abstract class AbstractOverlayRenderer
    {
        protected bool visible = false;

        public AbstractOverlayRenderer()
        {
            OverlayUI.Instace.RenderEvent += RenderEvent;
            this.UpdateConfiguration();
            this.UpdateVisibility();
        }

        public void RenderEvent(object src, RenderEventArgs args)
        {
            if(this.visible)
            {
                this.Draw(args.graphics, args.config, args.window, args.time, args.totalTime);
            }
        }

        public abstract void Draw(Direct2DRenderer graphics, DrawConfiguration config, DirectXOverlayWindow window, long time, long totalTime);

        public abstract void UpdateVisibility();

        public abstract void UpdateConfiguration();
    }
}
