﻿using System;
using System.ComponentModel;
using System.Windows.Controls;

namespace EDUtils.UI
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Page
    {
        public About()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string AppVersion {
            get {
                return EDUtilsApp.NAME+" "+EDUtilsApp.VERSION;
            }
        }
    }
}
