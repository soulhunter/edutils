﻿using EDUtils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EDUtils.UI
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class ConfigurationKeyDialog : Window
    {
        public KeyCombination keyConfig;
        private KeyCombination pressedKeyConfig;
        private bool isModifierEnabled = false;



        public ConfigurationKeyDialog(KeyCombination key=null, bool isModifierEnabled=false)
        {
            InitializeComponent();
            this.keyConfig = new KeyCombination();
            this.pressedKeyConfig = new KeyCombination();
            this.isModifierEnabled = isModifierEnabled;
        }

        private void OnContentRendered(object sender, EventArgs e)
        {
            //Init
            this.KeyDown += this.OnKeyDown;
            this.KeyUp += this.OnKeyUp;
        }

        private void OkButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            //keyConfig = new KeyConfig();
            this.DialogResult = false;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.LeftCtrl:
                case Key.RightCtrl:
                case Key.LeftShift:
                case Key.RightShift:
                case Key.LeftAlt:
                case Key.RightAlt:
                    if(pressedKeyConfig.IsEmpty())
                    {
                        if(isModifierEnabled)
                        {
                            ModifierKeys modKey = ModifierKeys.None;
                            switch(e.Key)
                            {
                                case Key.LeftCtrl:
                                case Key.RightCtrl:
                                    modKey = ModifierKeys.Control;
                                    break;
                                case Key.LeftShift:
                                case Key.RightShift:
                                    modKey = ModifierKeys.Shift;
                                    break;
                                case Key.LeftAlt:
                                case Key.RightAlt:
                                    modKey = ModifierKeys.Alt;
                                    break;
                                default:
                                    break;
                            }
                            pressedKeyConfig.modifier = modKey;
                        }
                        else
                        {
                            pressedKeyConfig.key = e.Key;
                        }
                    }
                    break;
                case Key.RWin:
                case Key.LWin:
                case Key.Escape:
                    pressedKeyConfig.key = Key.None;
                    break;
                default:
                    if(!pressedKeyConfig.HasKey())
                    {
                        pressedKeyConfig.key = e.Key;
                    }
                    break;
            }
            UpdateLabel(pressedKeyConfig);
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.LeftCtrl:
                case Key.RightCtrl:
                case Key.LeftShift:
                case Key.RightShift:
                case Key.LeftAlt:
                case Key.RightAlt:
                    ModifierKeys modKey = KeyCombination.ModifierFromKey(e.Key);
                    if(pressedKeyConfig.key==e.Key
                        || pressedKeyConfig.modifier==modKey)
                    {
                        if(pressedKeyConfig.HasKey())
                        {
                            this.keyConfig = pressedKeyConfig;
                        }
                        pressedKeyConfig = new KeyCombination();
                        UpdateLabel(this.keyConfig);
                    }
                    break;
                case Key.RWin:
                case Key.LWin:
                case Key.Escape:
                    break;
                default:
                    if(pressedKeyConfig.HasKey()
                        && !pressedKeyConfig.HasModifier()
                        && pressedKeyConfig.key==e.Key)
                    {
                        this.keyConfig = pressedKeyConfig;
                        pressedKeyConfig = new KeyCombination();
                        UpdateLabel(this.keyConfig);
                    }
                    break;
            }
        }

        public void UpdateLabel(KeyCombination keyConfig)
        {
            PressedKeys.Text = keyConfig.ToString();
        }
    }
}
