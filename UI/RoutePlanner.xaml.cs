﻿using EDUtils.Models;
using EDUtils.Services.RoutePlanner;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EDUtils.UI
{
    public partial class RoutePlanner : Page
    {
        public static readonly Color COLOR_PREVIOUS_SYSTEMS = Colors.GreenYellow;
        public static readonly Color COLOR_CURRENT_SYSTEM = Colors.GreenYellow;
        public static readonly Color COLOR_NEXT_SYSTEM = Colors.Yellow;

        public RoutePlannerService RoutePlannerService {
            get { return ((EDUtilsApp)EDUtilsApp.Current).routePlannerService; }
        }
        public string CurrentStarSystemName {
            get { return this.RoutePlannerService.CurrentStarSystem.name; }
        }

        public RoutePlanner()
        {
            InitializeComponent();
            RouteListView.DataContext = this;
            RoutePlannerService.PropertyChanged += OnRoutePlannerServicePropertyChanged;
            RoutePlannerService.Route.PropertyChanged += OnRoutePropertyChanged;
        }

        private void CalculateRouteAsync()
        {
            string from = this.FromSystem.Text;
            string to = this.ToSystem.Text;
            float shipJumpRange = this.ParseShipJumpRange(this.ShipJumpRange.Text);
            Task.Factory.StartNew(() => {
                    bool result = this.RoutePlannerService.TraceRouteAsync(from, to, shipJumpRange).Result;
                    if(!result)
                    {
                        MessageBox.Show(EDUtils.Resources.i18n.PlanningRouteFailedEnsureRouteValidStartEnd,
                                        EDUtils.Resources.i18n.Error,
                                        MessageBoxButton.OK,
                                        MessageBoxImage.Error);
                    }
                });
        }

        private void LoadFormData(string fieldName=null)
        {
            RoutePlannerService.PropertyChanged -= OnRoutePlannerServicePropertyChanged;
            RoutePlannerService.Route.PropertyChanged -= OnRoutePropertyChanged;
            EDUtilsApp.Current.Dispatcher.Invoke((System.Action)delegate {

                switch(fieldName)
                {
                    case "IsEnabled":
                    case "IsPlanning":
                        this.IsEnabled = !RoutePlannerService.IsPlanning;
                        break;
                    case "FromSystem":
                    case "CurrentStarSystem":
                        this.FromSystem.Text = this.RoutePlannerService.CurrentStarSystem?.Name ?? "";
                        break;
                    case "ToSystem":
                    case "TargetStarSystem":
                        this.ToSystem.Text = this.RoutePlannerService.TargetStarSystem?.Name ?? "";
                        break;
                    case "ShipJumpRange":
                        this.ShipJumpRange.Text = this.ParseShipJumpRange() + "";
                        break;
                    default:
                        this.IsEnabled = !RoutePlannerService.IsPlanning;
                        this.FromSystem.Text = this.RoutePlannerService.CurrentStarSystem?.Name ?? "";
                        this.ToSystem.Text = this.RoutePlannerService.TargetStarSystem?.Name ?? "";
                        this.ShipJumpRange.Text = this.ParseShipJumpRange() + "";
                        break;

                }
                RoutePlannerService.PropertyChanged += OnRoutePlannerServicePropertyChanged;
                RoutePlannerService.Route.PropertyChanged += OnRoutePropertyChanged;
            });
        }

        private float ParseShipJumpRange(string text=null)
        {
            float shipJumpRange = Properties.Settings.Default.ShipJumpRange;
            text = text?? this.ShipJumpRange.Text;
            if(!string.IsNullOrWhiteSpace(text))
            {
                float.TryParse(text, out shipJumpRange);
                if(shipJumpRange<10)
                {
                    shipJumpRange = Properties.Settings.Default.ShipJumpRange;
                }
            }
            return shipJumpRange;
        }

        private void CalculateRouteButtonClick(object sender, RoutedEventArgs e)
        {
            this.CalculateRouteAsync();
        }

        private void ResetButtonClick(object sender, RoutedEventArgs e)
        {
            this.LoadFormData();
        }

        private void OnRoutePlannerServicePropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            this.LoadFormData(args?.PropertyName);
        }

        private void OnRoutePropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if(args?.PropertyName=="CurrentIndex")
            {
                //Trigger the row reload in order to put the proper colors
                this.RouteListView.Items.Refresh();
            }
        }

        private void RouteListView_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if(this.RoutePlannerService?.Route!=null
                && e?.Row?.DataContext!=null
                && e.Row.DataContext is RouteStarSystem)
            {
                RouteStarSystem rowSystem = e.Row.DataContext as RouteStarSystem;
                if(rowSystem.RouteStep<this.RoutePlannerService.Route.CurrentIndex)
                {
                    e.Row.Background = new SolidColorBrush(COLOR_PREVIOUS_SYSTEMS);
                }
                else if(rowSystem.RouteStep==this.RoutePlannerService.Route.CurrentIndex)
                {
                    e.Row.Background = new SolidColorBrush(COLOR_CURRENT_SYSTEM);
                }
                else if(rowSystem.RouteStep==this.RoutePlannerService.Route.CurrentIndex+1)
                {
                    e.Row.Background = new SolidColorBrush(COLOR_NEXT_SYSTEM);
                }
            }

        }

        public bool Test
        {
            get { return true; }
        }
    }
}
