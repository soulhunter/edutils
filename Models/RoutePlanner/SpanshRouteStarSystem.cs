﻿
namespace EDUtils.Models.SpanshRoute
{
    public class SpanshRouteStarSystem : RouteStarSystem
    {
        public double distance_jumped = 0.0;
        //Distance to the next waypoint
        public double distance_left = 0.0;
        
        public bool neutron_star {
            get { return TYPE_NEUTRON.Equals(this.type, System.StringComparison.OrdinalIgnoreCase); }
            set { this.type = value? TYPE_NEUTRON : ""; }
        }
        
        public override double Distance {
            get { return this.distance_jumped; }
            set {
                this.distance_jumped = value;
                this.OnPropertyChanged("Distance");
            }
        }
    }
}
