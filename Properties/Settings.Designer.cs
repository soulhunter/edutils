﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EDUtils.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("30")]
        public float ShipJumpRange {
            get {
                return ((float)(this["ShipJumpRange"]));
            }
            set {
                this["ShipJumpRange"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("10")]
        public ushort LowFuelWarningLevel {
            get {
                return ((ushort)(this["LowFuelWarningLevel"]));
            }
            set {
                this["LowFuelWarningLevel"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("60")]
        public ushort RoutePlannerSpanshEfficiency {
            get {
                return ((ushort)(this["RoutePlannerSpanshEfficiency"]));
            }
            set {
                this["RoutePlannerSpanshEfficiency"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool RoutePlannerAutoCalculation {
            get {
                return ((bool)(this["RoutePlannerAutoCalculation"]));
            }
            set {
                this["RoutePlannerAutoCalculation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("97")]
        public ushort HotkeyRoutePlannerCalculate {
            get {
                return ((ushort)(this["HotkeyRoutePlannerCalculate"]));
            }
            set {
                this["HotkeyRoutePlannerCalculate"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("95")]
        public ushort HotkeyToggleOverlayUI {
            get {
                return ((ushort)(this["HotkeyToggleOverlayUI"]));
            }
            set {
                this["HotkeyToggleOverlayUI"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public ushort HotkeyModifierToggleOverlayUI {
            get {
                return ((ushort)(this["HotkeyModifierToggleOverlayUI"]));
            }
            set {
                this["HotkeyModifierToggleOverlayUI"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32, 32, 32, 32")]
        public global::System.Drawing.Color OverlayUIBackgroundColor {
            get {
                return ((global::System.Drawing.Color)(this["OverlayUIBackgroundColor"]));
            }
            set {
                this["OverlayUIBackgroundColor"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("252, 178, 17")]
        public global::System.Drawing.Color OverlayUIBorderColor {
            get {
                return ((global::System.Drawing.Color)(this["OverlayUIBorderColor"]));
            }
            set {
                this["OverlayUIBorderColor"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("252, 178, 17")]
        public global::System.Drawing.Color OverlayUIColor {
            get {
                return ((global::System.Drawing.Color)(this["OverlayUIColor"]));
            }
            set {
                this["OverlayUIColor"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Elite Dangerous\\EDLaunch.exe")]
        public string GameExePath {
            get {
                return ((string)(this["GameExePath"]));
            }
            set {
                this["GameExePath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Program Files (x86)\\Steam\\Steam.exe")]
        public string SteamExePath {
            get {
                return ((string)(this["SteamExePath"]));
            }
            set {
                this["SteamExePath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public double MainWindowTop {
            get {
                return ((double)(this["MainWindowTop"]));
            }
            set {
                this["MainWindowTop"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public double MainWindowLeft {
            get {
                return ((double)(this["MainWindowLeft"]));
            }
            set {
                this["MainWindowLeft"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public double MainWindowWidth {
            get {
                return ((double)(this["MainWindowWidth"]));
            }
            set {
                this["MainWindowWidth"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public double MainWindowHeight {
            get {
                return ((double)(this["MainWindowHeight"]));
            }
            set {
                this["MainWindowHeight"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public ushort MainWindowState {
            get {
                return ((ushort)(this["MainWindowState"]));
            }
            set {
                this["MainWindowState"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public ushort HotkeyModifierRoutePlannerCalculate {
            get {
                return ((ushort)(this["HotkeyModifierRoutePlannerCalculate"]));
            }
            set {
                this["HotkeyModifierRoutePlannerCalculate"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool OverlayUIShowRouteRemaining {
            get {
                return ((bool)(this["OverlayUIShowRouteRemaining"]));
            }
            set {
                this["OverlayUIShowRouteRemaining"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("875")]
        public ushort OverlayUIFuelLevelWarningX {
            get {
                return ((ushort)(this["OverlayUIFuelLevelWarningX"]));
            }
            set {
                this["OverlayUIFuelLevelWarningX"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("50")]
        public ushort OverlayUIFuelLevelWarningY {
            get {
                return ((ushort)(this["OverlayUIFuelLevelWarningY"]));
            }
            set {
                this["OverlayUIFuelLevelWarningY"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public ushort OverlayUIRouteInfoX {
            get {
                return ((ushort)(this["OverlayUIRouteInfoX"]));
            }
            set {
                this["OverlayUIRouteInfoX"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public ushort OverlayUIRouteInfoY {
            get {
                return ((ushort)(this["OverlayUIRouteInfoY"]));
            }
            set {
                this["OverlayUIRouteInfoY"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool OverlayUIEnabled {
            get {
                return ((bool)(this["OverlayUIEnabled"]));
            }
            set {
                this["OverlayUIEnabled"] = value;
            }
        }
    }
}
