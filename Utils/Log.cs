﻿using System;

namespace EDUtils.Utils
{
    class Log
    {
        public static readonly NLog.Logger LOGGER = NLog.LogManager.GetCurrentClassLogger();

        private static string PrepareMessage(string message, Type classType=null, string methodName=null)
        {
            return classType==null?
                    message
                    : (string.IsNullOrWhiteSpace(methodName)?
                        string.Format("{0} - {1}", classType.FullName, message)
                        : string.Format("{0}.{1}() - {2}", classType.FullName, methodName, message));
        }

        public static void Debug(string message, Type classType=null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Debug(logMessage);
        }

        public static void Info(string message, Type classType=null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Info(logMessage);
        }

        public static void Warn(string message, Type classType=null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Warn(logMessage);
        }

        public static void Warn(string message, Exception exception, Type classType =null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Warn(exception, logMessage);
        }

        public static void Error(string message, Type classType=null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Error(logMessage);
        }

        public static void Error(string message, Exception exception, Type classType=null, string methodName=null)
        {
            string logMessage = PrepareMessage(message, classType, methodName);
            LOGGER.Error(exception, logMessage);
        }
    }
}
