﻿using System;
using System.ComponentModel;

namespace EDUtils.Utils
{
    public abstract class AbstractNotifyPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if(this.PropertyChanged!=null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                });
            }
        }
    }
}
