﻿using EDUtils.Utils;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace EDUtils.Services
{
    class ProcessMonitor
    {
        public const string CONFIG_PROCESS_CHECK_TIME = "ProcessCheckTime";
        public const int CONFIG_PROCESS_CHECK_TIME_DEFAULT = 1000;
        public static readonly int PROCESS_CHECK_TIME = AppConfig.GetAppValue(CONFIG_PROCESS_CHECK_TIME, CONFIG_PROCESS_CHECK_TIME_DEFAULT);
        public const string CONFIG_PLAYING_PROCESS_CHECK_TIME = "PlayingProcessCheckTime";
        public const int CONFIG_PLAYING_PROCESS_CHECK_TIME_DEFAULT = 5000;
        public static readonly int PLAYING_PROCESS_CHECK_TIME = AppConfig.GetAppValue(CONFIG_PLAYING_PROCESS_CHECK_TIME, CONFIG_PLAYING_PROCESS_CHECK_TIME_DEFAULT);

        public const string CONFIG_STEAM_PARAMS = "SteamParams";
        public const string CONFIG_STEAM_PARAMS_DEFAULT = "-applaunch 359320";
        public static readonly string STEAM_PARAMS = AppConfig.GetAppValue(CONFIG_STEAM_PARAMS, CONFIG_STEAM_PARAMS_DEFAULT);

        public const string GAME_PROCCESS_NAME = "EliteDangerous64";
        public const string LAUNCHER_PROCCESS_NAME = "EDLaunch";

        public static readonly ProcessHandler GameProcess = new ProcessHandler(GAME_PROCCESS_NAME);
        public static readonly ProcessHandler LauncherProcess = new ProcessHandler(LAUNCHER_PROCCESS_NAME);

        public static bool IsRunning {
            get {
                return GameProcess.IsRunning
                    || LauncherProcess.IsRunning;
            }
        }

        private static bool running = false;
        public static event EventHandler ProcessChecked;

        private static Task task = new Task(() => {
                running = true;
                while(running)
                {
                    CheckProcesses();
                    if(ProcessChecked!=null)
                    {
                        EventArgs ev = new EventArgs();
                        ProcessChecked(null, ev);
                    }
                    task.Wait(GameProcess.IsRunning? CONFIG_PLAYING_PROCESS_CHECK_TIME_DEFAULT : PROCESS_CHECK_TIME);
                }
            });

        public static bool CanLaunchSteam {
            get {
                return !string.IsNullOrWhiteSpace(Properties.Settings.Default.SteamExePath)
                    && File.Exists(Properties.Settings.Default.SteamExePath);
            }
        }

        public static bool CanLaunchStandalone {
            get {
                return !string.IsNullOrWhiteSpace(Properties.Settings.Default.GameExePath)
                    && File.Exists(Properties.Settings.Default.GameExePath);
            }
        }

        public static bool LaunchSteam()
        {
            return !IsRunning
                && CanLaunchSteam
                && LaunchProcess(Properties.Settings.Default.SteamExePath, STEAM_PARAMS);
        }

        public static bool LaunchStandalone()
        {
            return !IsRunning
                && CanLaunchStandalone
                && LaunchProcess(Properties.Settings.Default.GameExePath);
        }

        private static bool LaunchProcess(string path, string args=null)
        {
            bool success = false;
            try
            {
                System.Diagnostics.Process proc = System.Diagnostics.Process.Start(path, args);
                success = proc.Responding;
                if(success)
                {
                    CheckProcesses();
                }
            }
            catch(Exception exception)
            {
                Log.Error(string.Format("Error launching process {0} {1}", path, args), exception, Type.GetType("ProcessMonitor"), string.Format("LaunchProcess({0}, {1})", path, args));
            }
            return success;
        }

        public static void Start()
        {
            if(!running)
            {
                task.Start();
            }
            GameProcess.ProcessStarted -= OnGameProcessStarted;
            GameProcess.ProcessExited -= OnProcessExited;
            GameProcess.ProcessStarted += OnGameProcessStarted;
            GameProcess.ProcessExited += OnProcessExited;
        }

        public static void Stop()
        {
            running = false;
        }

        private static void CheckProcesses()
        {
            LauncherProcess.UpdateProcess();
            GameProcess.UpdateProcess();
        }

        private static void OnGameProcessStarted(object sender, EventArgs ev)
        {
            if(GameProcess.IsRunning)
            {
                //Stop();
            }
        }

        private static void OnProcessExited(object sender, EventArgs ev)
        {
            if(!GameProcess.IsRunning)
            {
                //Start();
            }
        }
    }
}
