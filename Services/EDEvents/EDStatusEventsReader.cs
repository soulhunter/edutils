﻿using EDUtils.Models.EDEvents;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EDUtils.Services.EDEvents
{
    /// <summary>
    /// Reads and handles the game status events.
    /// </summary>
    class EDStatusEventsReader : EDLogsReader<EDStatusEvent>
    {
        /// <summary>
        /// Status log file name.
        /// </summary>
        public const string FILE_NAME = "status.json";

        /// <summary>
        /// Initializes the status events reader.
        /// </summary>
        public EDStatusEventsReader() : base()
        {

        }

        /// <summary>
        /// Reads the current status from file.
        /// </summary>
        /// <returns>List of one status event.</returns>
        public override List<EDStatusEvent> ReadEvents()
        {
            this.ResetFile();
            List<EDStatusEvent> events = base.ReadEvents();
            return events;
        }

        /// <summary>
        /// Reads the current status from file.
        /// </summary>
        /// <returns>The current status event.</returns>
        public EDStatusEvent ReadStatus()
        {
            List<EDStatusEvent> events = this.ReadEvents();
            return events.LastOrDefault();
        }

        /// <summary>
        /// Returns the status log file info.
        /// </summary>
        /// <returns>Status log file info.</returns>
        protected override FileInfo GetLogFile()
        {
            IEnumerable<FileInfo> files = LOGS_DIRECTORY.EnumerateFiles(FILE_NAME);
            files = files.OrderByDescending(x => x.CreationTime);
            FileInfo file = files.FirstOrDefault();
            return file;
        }
    }
}
