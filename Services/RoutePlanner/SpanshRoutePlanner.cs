﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using EDUtils.Models;
using EDUtils.Models.SpanshRoute;
using EDUtils.Utils;
using Flurl;
using Flurl.Http;

namespace EDUtils.Services
{
    class SpanshRoutePlanner : AbstractRoutePlanner
    {
        public const string SPANSH_URL = "https://www.spansh.co.uk/plotter/results/{4}?from={0}&to={1}&range={2}&efficiency={3}";
        public const string REQUEST_API_URL = "https://spansh.co.uk/api/";
        public const string CONFIG_QUEUE_WAIT_TIME = "SpanshQueueWaitTime";
        public const int CONFIG_QUEUE_WAIT_TIME_DEFAULT = 1000;
        public static readonly int QUEUE_WAIT_TIME = AppConfig.GetAppValue(CONFIG_QUEUE_WAIT_TIME, CONFIG_QUEUE_WAIT_TIME_DEFAULT);

        public static readonly string REQUEST_ROUTE_URL = REQUEST_API_URL+"route/";
        public static readonly string REQUEST_RESULTS_URL = REQUEST_API_URL + "results/";

        public const string REQUEST_METHOD = "POST";
        public const string REQUEST_CONTENT_TYPE = "application/x-www-form-urlencoded; charset=UTF-8";
        public const string PARAM_FROM = "from";
        public const string PARAM_TO = "to";
        public const string PARAM_WAYPOINT = "via";
        public const string PARAM_RANGE = "range";
        public const string PARAM_EFFICIENCY = "efficiency";

        public SpanshRoutePlanner()
        {

        }

        public override async Task<List<RouteStarSystem>> TraceRouteAsync(string from, string to, float jumpRange=0, string[] waypoints=null)
        {
            Log.Info(string.Format("Requested route from {0} to {1} with a jump range of {2} and {3} waypoints", from, to, jumpRange, waypoints?.Length?? 0), this.GetType());
            if(jumpRange<=0)
            {
                jumpRange = Properties.Settings.Default.ShipJumpRange;
            }
            if(jumpRange<10)
            {
                throw new ArgumentOutOfRangeException("ShipJumpRange should be >= 10");
            }
            int efficiency = Properties.Settings.Default.RoutePlannerSpanshEfficiency;
            if(efficiency<1 || efficiency>100)
            {
                throw new ArgumentOutOfRangeException("SpanshRouteEfficiency should be between 1 and 100");
            }
            List<KeyValuePair<string, string>> requestParams = new List<KeyValuePair<string, string>>();
            requestParams.Add(new KeyValuePair<string, string>(PARAM_FROM, from));
            requestParams.Add(new KeyValuePair<string, string>(PARAM_TO, to));
            requestParams.Add(new KeyValuePair<string, string>(PARAM_RANGE, Math.Truncate(jumpRange).ToString()));
            requestParams.Add(new KeyValuePair<string, string>(PARAM_EFFICIENCY, efficiency.ToString()));
            if(waypoints!=null)
            {
                foreach(string waypoint in waypoints)
                {
                    requestParams.Add(new KeyValuePair<string, string>(PARAM_WAYPOINT, waypoint));
                }
            }
            HttpContent requestContent = new FormUrlEncodedContent(requestParams);
            SpanshRouteJob routeJob = await REQUEST_ROUTE_URL
                                    .PostAsync(requestContent)
                                    .ReceiveJson<SpanshRouteJob>();
            SpanshRouteResult results = await WaitForJobResponse(routeJob);
            List<SpanshRouteStarSystem> spanshRoute = results.system_jumps;
            Log.Info(string.Format("Route from {0} to {1} calculated: {2} route checkpoints", from, to, spanshRoute.Count), this.GetType());
            List<RouteStarSystem> route = new List<RouteStarSystem>(spanshRoute);
            RouteStarSystem.CalculateRouteData(route);
            return route;
        }

        public async Task<SpanshRouteResult> WaitForJobResponse(SpanshRouteJob routeJob)
        {
            SpanshRouteResult routeResult = null;
            if(routeJob.status==SpanshRouteJob.STATUS_OK
                && routeJob.result!=null)
            {
                routeResult = routeJob.result;
            }
            else if(routeJob.status==SpanshRouteJob.STATUS_QUEUED)
            {
                try {
                    System.Threading.Thread.Sleep(QUEUE_WAIT_TIME);
                } catch(Exception exception) {
                    Log.Error("Exception waiting for Spansh queued job", exception, this.GetType());
                }
                Log.Info("Requesting results for route", this.GetType());
                SpanshRouteJob routeJobResponse = await REQUEST_RESULTS_URL.AppendPathSegment(routeJob.job)
                                                        .GetJsonAsync<SpanshRouteJob>();
                routeResult = await WaitForJobResponse(routeJobResponse);
            }
            return routeResult;
        }
    }
}
