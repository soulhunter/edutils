﻿using Clipperino;
using EDUtils.Models;
using EDUtils.Services.EDEvents;
using EDUtils.Utils;
using System;
using System.Collections.Generic;

namespace EDUtils.Services.RoutePlanner
{
    public class RoutePlannerService : AbstractNotifyPropertyChanged
    {
        private StarSystem currentStarSystem = null;
        private StarSystem targetStarSystem = null;
        public StarSystem CurrentStarSystem {
            get { return this.currentStarSystem; }
            set {
                this.currentStarSystem = value;
                this.OnPropertyChanged("CurrentStarSystem");
                this.OnPropertyChanged("CurrentStarSystemName");
            }
        }
        public StarSystem TargetStarSystem {
            get { return this.targetStarSystem; }
            set {
                this.targetStarSystem = value;
                this.OnPropertyChanged("TargetStarSystem");
            }
        }

        private Route route = new Route();
        public Route Route {
            get { return this.route; }
            set {
                this.route = value;
                this.OnPropertyChanged("Route");
            }
        }
        public bool HasRoute {
            get { return this.route!=null && this.route.Count>0; }
        }

        private bool isPlanning = false;
        public bool IsPlanning {
            get { return this.isPlanning; }
        }

        public RoutePlannerService()
        {
            this.Init();
        }

        protected void Init()
        {
            EDEventMonitor.OnFSDTargetJournalEvent += this.OnFSDTargetEvent;
            EDEventMonitor.OnFSDJumpJournalEvent += this.OnLocationEvent;
            EDEventMonitor.OnLocationJournalEvent += this.OnLocationEvent;
        }

        public async System.Threading.Tasks.Task<bool> TraceRouteAsync(string from, string to, float jumpRange=0.0f)
        {
            bool result = false;
            if(!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to))
            {
                if(!this.isPlanning)
                {
                    this.isPlanning = true;
                    this.OnPropertyChanged("IsPlanning");
                    SpanshRoutePlanner planner = new SpanshRoutePlanner();
                    try
                    {
                        List<RouteStarSystem> routeItems = await planner.TraceRouteAsync(from, to, jumpRange);
                        EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate{
                            this.Route.Reset(routeItems);
                            if(this.Route.NextStarSystem!=null)
                            {
                                Clipboard.Save(this.Route.NextStarSystem.Name);
                            }
                        });
                        result = routeItems.Count>0;
                    }
                    catch(Exception exception)
                    {
                        Log.Error(string.Format("Error planning route:", from, to, jumpRange), exception, this.GetType(), string.Format("TraceRouteAsync({0}, {1}, {2})", from, to, jumpRange));
                    }
                    finally
                    {
                        this.isPlanning = false;
                        this.OnPropertyChanged("IsPlanning");
                    }
                }
                else
                {
                    Log.Warn(string.Format("Please wait to finish the currently calculating route to start a new one", from, to, jumpRange), this.GetType(), string.Format("TraceRouteAsync({0}, {1}, {2})", from, to, jumpRange));
                }
            }
            else
            {
                Log.Warn(string.Format("Missing some required parameters", from, to, jumpRange), this.GetType(), string.Format("TraceRouteAsync({0}, {1}, {2})", from, to, jumpRange));
            }
            return result;
        }

        public async System.Threading.Tasks.Task<bool> TraceRouteAsync()
        {
            return await TraceRouteAsync(this.CurrentStarSystem?.Name, this.TargetStarSystem?.Name);
        }

        public void OnLocationEvent(object sender, EDEventMonitor.JournalEventArgs eventArgs)
        {
            if(!string.IsNullOrWhiteSpace(eventArgs.journal.StarSystem)
                && (string.IsNullOrWhiteSpace(this.CurrentStarSystem?.Name)
                    || !eventArgs.journal.StarSystem.Equals(this.CurrentStarSystem.Name, StringComparison.OrdinalIgnoreCase)))
            {
                this.CurrentStarSystem = new StarSystem(eventArgs.journal.StarSystem);
                if(this.Route!=null)
                {
                    this.Route.SetCurrentStarSystem(this.currentStarSystem);
                    if(this.Route.NextStarSystem!=null)
                    {
                        Clipboard.Save(this.Route.NextStarSystem.Name);
                    }
                }
                if(!string.IsNullOrWhiteSpace(this.TargetStarSystem?.Name)
                    && this.CurrentStarSystem.Name.Equals(this.TargetStarSystem.Name, StringComparison.OrdinalIgnoreCase))
                {
                    this.TargetStarSystem = null;
                }
            }
        }

        public void OnFSDTargetEvent(object sender, EDEventMonitor.JournalEventArgs eventArgs)
        {
            if(!string.IsNullOrWhiteSpace(eventArgs.journal.Name)
                && (string.IsNullOrWhiteSpace(this.TargetStarSystem?.Name)
                    || !eventArgs.journal.Name.Equals(this.TargetStarSystem.Name, StringComparison.OrdinalIgnoreCase)))
            {
                this.TargetStarSystem = new StarSystem(eventArgs.journal.Name);
            }
        }
    }
}
