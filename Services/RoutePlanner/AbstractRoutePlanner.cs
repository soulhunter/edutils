﻿using EDUtils.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDUtils.Services
{
    abstract class AbstractRoutePlanner
    {
        public async Task<List<RouteStarSystem>> TraceRouteAsync(StarSystem from, StarSystem to, StarShip ship, List<StarSystem> waypoints=null)
        {
            string[] waypointNames = waypoints!=null? waypoints.Select(x => x.name).ToArray() : null;
            return await TraceRouteAsync(from.name, to.name, ship.jumpRange, waypointNames);
        }
        public abstract Task<List<RouteStarSystem>> TraceRouteAsync(string from, string to, float jumpRange=0, string[] waypoints=null);
    }
}
