﻿using System.IO;
using System.Xml;

namespace EDUtils.Services.Accounts
{
    public class EDAccountConfiguration
    {
        public const string FILENAME_DEFAULT = "user.config";
        public const string FILENAME_FORMAT = "{0}_"+FILENAME_DEFAULT;
        public const string XML_USERNAME_PATH = "//userSettings//setting[@name='UserName']/value";

        public readonly FileInfo FileInfo;
        public readonly string username;
        public string Username {
            get{
                return this.username; 
            }
        }

        public EDAccountConfiguration(FileInfo fileInfo)
        {
            this.FileInfo = fileInfo;
            this.username = GetUsername(this.FileInfo);
            if(string.IsNullOrWhiteSpace(this.username))
            {
                throw new IOException(string.Format("Username is empty in file {0}", fileInfo.FullName));
            }
        }

        public string BuildFilename()
        {
            return BuildFilename(this.username);
        }
        public string BuildFilePath()
        {
            return this.FileInfo.DirectoryName+"\\"+BuildFilename(this.username);
        }

        public static string BuildFilename(string username)
        {
            return string.Format(FILENAME_FORMAT, username);
        }

        public static string GetUsername(FileInfo fileInfo)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(fileInfo.FullName);
            XmlNode usernameNode = doc.DocumentElement.SelectSingleNode(XML_USERNAME_PATH);
            string username = usernameNode.InnerText;
            return username;
        }
    }
}
