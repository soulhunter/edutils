﻿
using EDUtils.Utils;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace EDUtils.Services.Accounts
{
    public class EDAccountsService : List<EDAccountConfiguration>, INotifyPropertyChanged, INotifyCollectionChanged
    {
        public const string CONFIG_ACCOUNT_CHECK_TIME = "AccountCheckTime";
        public const int CONFIG_ACCOUNT_CHECK_TIME_DEFAULT = 5000;
        public static readonly int ACCOUNT_CHECK_TIME = AppConfig.GetAppValue(CONFIG_ACCOUNT_CHECK_TIME, CONFIG_ACCOUNT_CHECK_TIME_DEFAULT);

        public static readonly DirectoryInfo ACCOUNT_APPDATA = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+"\\Frontier_Developments_Ltd");

        public static readonly EDAccountsService Instace = new EDAccountsService();

        private EDAccountConfiguration currentAccount;
        public EDAccountConfiguration CurrentAccount {
            get {
                return this.currentAccount;
            }
        }

        public bool CanSaveCurrentAccount {
            get {
                return this.currentAccount!=null;
            }
        }

        public bool CanDeleteCurrentAccount {
            get {
                return this.currentAccount!=null
                    && this.Any(x=>x.Username.Equals(this.currentAccount.Username, StringComparison.OrdinalIgnoreCase));
            }
        }

        public bool CanChangeCurrentAccount {
            get {
                return !ProcessMonitor.IsRunning;
            }
        }

        private EDAccountsService() : base()
        {
            ProcessMonitor.ProcessChecked += OnProcessChecked;
        }

        public void OnProcessChecked(object source, EventArgs ev)
        {
            if(!ProcessMonitor.GameProcess.IsRunning)
            {
                this.UpdateAccounts();
            }
        }

        public void UpdateAccounts()
        {
            try
            {
                EDAccountConfiguration newCurrent = GetCurrentProfileConfiguration();
                List<EDAccountConfiguration> newProfiles = GetProfilesConfiguration();
                if(!string.Equals(this.currentAccount?.Username, newCurrent?.Username, StringComparison.OrdinalIgnoreCase))
                {
                    this.currentAccount = newCurrent;
                    this.OnPropertyChanged("CurrentAccount");
                    this.OnPropertyChanged("CanSaveCurrentAccount");
                    this.OnPropertyChanged("CanDeleteCurrentAccount");
                    this.OnPropertyChanged("CanChangeCurrentAccount");
                }
                if(!this.Select(x=>x.Username).SequenceEqual(newProfiles.Select(x=>x.Username)))
                {
                    this.Clear();
                    this.AddRange(GetProfilesConfiguration());
                    this.OnCollectionChanged();
                }
            }
            catch(Exception exception)
            {
                Log.Error("Exception trying to get list of profiles", exception, this.GetType(), "GetProfilesFileInfo()");
            }
        }

        public void SaveCurrentAccount()
        {
            if(this.CanSaveCurrentAccount)
            {
                string sourcePath = this.currentAccount.FileInfo.FullName;
                string destinationPath = this.currentAccount.BuildFilePath();
                File.Copy(sourcePath, destinationPath, true);
                this.UpdateAccounts();
            }
        }

        public void DeleteCurrentAccount()
        {
            if(this.CanDeleteCurrentAccount)
            {
                EDAccountConfiguration account = this.Where(x=>x.Username.Equals(this.currentAccount?.Username, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if(account!=null)
                {
                    File.Delete(account.FileInfo.FullName);
                    this.UpdateAccounts();
                }
            }
        }

        public void ChangeCurrentAccount(string username)
        {
            EDAccountConfiguration account = this.Where(x => x.Username.Equals(username, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            this.ChangeCurrentAccount(account);
        }

        public void ChangeCurrentAccount(EDAccountConfiguration account)
        {
            if(this.CanChangeCurrentAccount
                && account!=null
                && !account.Username.Equals(this.currentAccount?.Username, StringComparison.OrdinalIgnoreCase))
            {
                string sourcePath = account.FileInfo.FullName;
                string destinationPath = this.currentAccount.FileInfo.FullName;
                File.Copy(sourcePath, destinationPath, true);
                this.UpdateAccounts();
            }
        }

        private static FileInfo GetCurrentProfileFileInfo()
        {
            FileInfo file = null;
            try
            {
                IEnumerable<DirectoryInfo> directories = ACCOUNT_APPDATA.EnumerateDirectories("EDLaunch.exe_*").SelectMany(x=>x.EnumerateDirectories());
                IEnumerable<FileInfo> configFiles = directories.SelectMany(x=>x.EnumerateFiles(EDAccountConfiguration.FILENAME_DEFAULT));
                file = configFiles.OrderByDescending(x=>x.CreationTimeUtc).FirstOrDefault();
            }
            catch(Exception exception)
            {
                Log.Error("Exception trying to get current profile", exception, Type.GetType("EDAccountsConfiguration"), "GetCurrentProfileFileInfo()");
            }
            return file;
        }

        private static IEnumerable<FileInfo> GetProfilesFileInfo()
        {
            IEnumerable<FileInfo> profileFiles = new List<FileInfo>();
            FileInfo profileFile = GetCurrentProfileFileInfo();
            if(profileFile!=null)
            {
                try
                {
                    profileFiles = profileFile.Directory.EnumerateFiles(string.Format(EDAccountConfiguration.FILENAME_FORMAT, "*"));
                }
                catch(Exception exception)
                {
                    Log.Error("Exception trying to get list of profiles", exception, Type.GetType("EDAccountsConfiguration"), "GetProfilesFileInfo()");
                }
            }
            return profileFiles;
        }

        private static EDAccountConfiguration GetCurrentProfileConfiguration()
        {
            EDAccountConfiguration profileConfiguration = null;
            try
            {
                FileInfo profileFile = GetCurrentProfileFileInfo();
                profileConfiguration = new EDAccountConfiguration(profileFile);
            }
            catch(Exception exception)
            {
                Log.Error("Exception trying to get list of profiles", exception, Type.GetType("EDAccountsConfiguration"), "GetCurrentProfileConfiguration()");
            }
            return profileConfiguration;
        }

        private static List<EDAccountConfiguration> GetProfilesConfiguration()
        {
            List<EDAccountConfiguration> profilesConfiguration = new List<EDAccountConfiguration>();
            IEnumerable<FileInfo> profileFiles = GetProfilesFileInfo();
            foreach(FileInfo profileFile in profileFiles)
            {
                try
                {
                    profilesConfiguration.Add(new EDAccountConfiguration(profileFile));
                }
                catch(Exception exception)
                {
                    Log.Error("Exception trying to get list of profiles", exception, Type.GetType("EDAccountsConfiguration"), "GetProfilesConfiguration()");
                }
            }
            return profilesConfiguration;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged!=null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                });
            }
        }
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public void OnCollectionChanged(NotifyCollectionChangedAction action=NotifyCollectionChangedAction.Reset)
        {
            if(CollectionChanged!=null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    NotifyCollectionChangedEventArgs ev = new NotifyCollectionChangedEventArgs(action);
                    CollectionChanged(this, ev);
                });
            }
        }
    }
}
